from django.forms import ModelForm, TextInput
from .models import Color


class ColorModelForm(ModelForm):
    class Meta:
        model = Color
        fields = "__all__"
        widgets = {
            "color_code": TextInput(attrs={'type': 'color'})
        }
