from django.urls import path
from .views import (ProductListApiView, ProductDetailApiView, CategoryListApiView,
                    CategoryDetailApiView, CallbackCreateApiView, PromotionListApiView,
                    CommentListApiView, ClientsListApiView, OrderCreateApiView)

urlpatterns = [
    path('products/', ProductListApiView.as_view(), name='product-list'),
    path('products/<int:pk>/', ProductDetailApiView.as_view(), name='product-detail'),
    path('categories/', CategoryListApiView.as_view(), name='category-list'),
    path('categories/<int:pk>/', CategoryDetailApiView.as_view(), name='category-detail'),
    path('clients/', ClientsListApiView.as_view(), name='client-list'),
    path('comments/', CommentListApiView.as_view(), name='comment-create'),
    path('promotions/', PromotionListApiView.as_view(), name='promotion-list'),

    path('callbacks/', CallbackCreateApiView.as_view(), name='callback-create'),
    path('order/', OrderCreateApiView.as_view(), name='order-create'),
]
